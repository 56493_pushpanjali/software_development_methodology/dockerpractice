const EmployeePage = () => {
    return (
        <div>
        <h1>Employee List</h1>
        <table className="table table-stripped">
            <thead>
            <tr>
                <th>EmpId</th>
                <th>Name</th>
                <th>Salary</th>
                <th>Age</th>
            </tr>
            </thead>

            <tbody>
            
                <tr>
                    <td>1</td>
                    <td>Prajakta</td>
                    <td>5000.50</td>
                    <td>24</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Pushpanjali</td>
                    <td>4000.50</td>
                    <td>23</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Sanket</td>
                    <td>3000.50</td>
                    <td>22</td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>aaaa</td>
                    <td>1000.50</td>
                    <td>21</td>
                </tr>
            
            </tbody>
        </table>
        </div>
  )
}

export default EmployeePage
