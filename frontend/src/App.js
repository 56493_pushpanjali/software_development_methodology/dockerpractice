import './App.css';
import EmployeePage from './pages/employee';

function App() {
  return (
    <div className="container">
      <div className="col"></div>  
      <div className="col"><EmployeePage/></div>
      <div className="col"></div> 
    </div>
  );
}

export default App;
